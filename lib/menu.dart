import 'package:flutter/material.dart';

import 'pages/keuangan.dart';
import 'pages/dukungan.dart';
import 'pages/kegiatan.dart';
import 'pages/percakapan.dart';
import 'pages/statistik.dart';
import 'pages/penugasan.dart';
import 'pages/quickcount.dart';
import 'pages/team.dart';

class MenuPage extends StatefulWidget {
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
            gradient: new LinearGradient(
                colors: [
                  Color(0xFFFFB74D),
                  Color(0xFFFF7043),
                  Color(0xFFFFA726),
                ],
                begin: FractionalOffset.topLeft,
                end: FractionalOffset.bottomLeft,
                stops: [0.0, 0.5, 1.0],
                tileMode: TileMode.clamp)),
        child: new ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
              child: new Container(
                child: new Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        new Expanded(
                          child: new Container(
                            height: 120.0,
                            decoration: new BoxDecoration(
                                boxShadow: <BoxShadow>[
                                  new BoxShadow(
                                    color: Colors.black12,
                                    offset: new Offset(0.0, 0.0),
                                    blurRadius: 1.0,
                                  ),
                                ],
                                gradient: new LinearGradient(
                                  colors: [
                                    Color(0xFFFF7043),
                                    Color(0xFFE65100),
                                  ],
                                  begin: FractionalOffset.topRight,
                                  end: FractionalOffset.bottomLeft,
                                  stops: [0.0, 1.0],
                                  tileMode: TileMode.clamp,
                                )),
                            child: new Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 10.0, right: 5.0),
                                      child: new Icon(
                                        Icons.calendar_today,
                                        color: Colors.white,
                                        size: 60.0,
                                      ),
                                    ),
                                    new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        new Text(
                                          'Menuju',
                                          style: new TextStyle(
                                            fontSize: 10.0,
                                            color: Colors.white54,
                                          ),
                                        ),
                                        new Text(
                                          'Pemunggutan Suara',
                                          style: new TextStyle(
                                            fontSize: 10.0,
                                            color: Colors.white54,
                                          ),
                                        ),
                                        new Text(
                                          'H-214',
                                          style: new TextStyle(
                                              fontSize: 35.0,
                                              color: Colors.greenAccent,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 5.0, right: 5.0),
                        ),
                        new Expanded(
                          child: new Container(
                            height: 120.0,
                            child: new Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Expanded(
                                  child: new Container(
                                    decoration: new BoxDecoration(
                                        boxShadow: <BoxShadow>[
                                          new BoxShadow(
                                            color: Colors.black12,
                                            offset: new Offset(0.0, 0.0),
                                            blurRadius: 1.0,
                                          ),
                                        ],
                                        gradient: new LinearGradient(
                                          colors: [
                                            Color(0xFFCE93D8),
                                            Color(0xFF9C27B0),
                                          ],
                                          begin: FractionalOffset.topRight,
                                          end: FractionalOffset.bottomLeft,
                                          stops: [0.0, 1.0],
                                          tileMode: TileMode.clamp,
                                        )),
                                    child: new Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        new Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8.0,
                                                  left: 2.0,
                                                  bottom: 2.0),
                                              child: new Text(
                                                'Dukungan Tercapai',
                                                style: new TextStyle(
                                                    fontSize: 10.0,
                                                    color: Colors.white70),
                                              ),
                                            ),
                                            new Text(
                                              '225.025',
                                              style: new TextStyle(
                                                  fontSize: 25.0,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 40.0),
                                          child: new Icon(
                                            Icons.show_chart,
                                            color: Colors.white,
                                            size: 35.0,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: new Container(
                                    decoration: new BoxDecoration(
                                        color: Color(0xFF7B1FA2)),
                                    child: new Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              right: 5.0,
                                              bottom: 20.0,
                                              left: 10.0),
                                          child: new Text(
                                            'Target',
                                            style: new TextStyle(
                                                fontSize: 13.0,
                                                color: Colors.white70),
                                          ),
                                        ),
                                        new Text(
                                          '485.000',
                                          style: new TextStyle(
                                              fontSize: 25.0,
                                              color: Colors.yellow,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Stack(
              children: <Widget>[
                Container(
                  decoration: new BoxDecoration(
                    color: Colors.white30
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 5.0),
                    child: new Container(
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              new Expanded(
                                child: new Container(
                                  height: 100.0,
                                  width: 100.0,
                                  decoration: new BoxDecoration(
                                    color: Color(0xFF480F48),
/*                                 gradient: new LinearGradient(
                                    colors: [
                                      Color(0xFF9C27B0),
                                      Color(0xFF542754),
                                    ],
                                    begin: FractionalOffset.topRight,
                                    end: FractionalOffset.bottomLeft,
                                    stops: [0.0, 0.6],
                                    tileMode: TileMode.clamp,
                                  ) */
                                  ),
                                  child: new FlatButton(
                                    onPressed: () => Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new KeuanganPage())),
                                    child: new Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(bottom: 5.0),
                                          child: new Icon(
                                            Icons.attach_money,
                                            color: Colors.white,
                                            size: 40.0,
                                          ),
                                        ),
                                        new Text(
                                          'Keuangan',
                                          style: new TextStyle(
                                            color: Colors.white,
                                            fontSize: 12.0
                                            ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 5.0, right: 5.0),
                              ),
                              new Expanded(
                                child: new Container(
                                  height: 100.0,
                                  width: 100.0,
                                  decoration: new BoxDecoration(
                                    color: Color(0xFF480F48),
                                  ),
                                  child: new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(bottom: 5.0),
                                        child: new Icon(
                                          Icons.thumb_up,
                                          color: Colors.white,
                                          size: 40.0,
                                        ),
                                      ),
                                      new Text(
                                        'Dukungan',
                                        style: new TextStyle(color: Colors.white),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 5.0, right: 5.0),
                              ),
                              new Expanded(
                                child: new Container(
                                  height: 100.0,
                                  width: 100.0,
                                  decoration: new BoxDecoration(
                                    color: Color(0xFF480F48),
                                  ),
                                  child: new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(bottom: 5.0),
                                        child: new Icon(
                                          Icons.timelapse,
                                          color: Colors.white,
                                          size: 40.0,
                                        ),
                                      ),
                                      new Text(
                                        'Kegiatan',
                                        style: new TextStyle(color: Colors.white),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 5.0, right: 5.0),
                              ),
                              new Expanded(
                                child: new Container(
                                  height: 100.0,
                                  width: 100.0,
                                  decoration: new BoxDecoration(
                                    color: Color(0xFF480F48),
                                  ),
                                  child: new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(bottom: 5.0),
                                        child: new Icon(
                                          Icons.chat,
                                          color: Colors.white,
                                          size: 40.0,
                                        ),
                                      ),
                                      new Text(
                                        'Percakapan',
                                        style: new TextStyle(color: Colors.white),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              decoration: new BoxDecoration(
                color: Colors.white30
              ),
              height: 110.0,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: new Container(
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          new Expanded(
                            child: new Container(
                              height: 100.0,
                              width: 100.0,
                              decoration: new BoxDecoration(
                                color: Color(0xFF480F48),
                              ),
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 5.0),
                                    child: new Icon(
                                      Icons.pie_chart,
                                      color: Colors.white,
                                      size: 40.0,
                                    ),
                                  ),
                                  new Text(
                                    'Statistik',
                                    style: new TextStyle(color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 5.0, right: 5.0),
                          ),
                          new Expanded(
                            child: new Container(
                              height: 100.0,
                              width: 100.0,
                              decoration: new BoxDecoration(
                                color: Color(0xFF480F48),
                              ),
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 5.0),
                                    child: new Icon(
                                      Icons.bookmark_border,
                                      color: Colors.white,
                                      size: 40.0,
                                    ),
                                  ),
                                  new Text(
                                    'Penugasan',
                                    style: new TextStyle(color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 5.0, right: 5.0,),
                          ),
                          new Expanded(
                            child: new Container(
                              height: 100.0,
                              width: 100.0,
                              decoration: new BoxDecoration(
                                color: Color(0xFF480F48),
                              ),
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 5.0),
                                    child: new Icon(
                                      Icons.timer,
                                      color: Colors.white,
                                      size: 40.0,
                                    ),
                                  ),
                                  new Text(
                                    'Quick Count',
                                    style: new TextStyle(color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 5.0, right: 5.0),
                          ),
                          new Expanded(
                            child: new Container(
                              height: 100.0,
                              width: 100.0,
                              decoration: new BoxDecoration(
                                color: Color(0xFF480F48),
                              ),
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 5.0),
                                    child: new Icon(
                                      Icons.group,
                                      color: Colors.white,
                                      size: 40.0,
                                    ),
                                  ),
                                  new Text(
                                    'Team',
                                    style: new TextStyle(color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 3.0),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              child: new Container(
                decoration: new BoxDecoration(
                  color: Color(0xFF006621),
                ),
                height: 70.0,
                child: ListView(
                  children: <Widget>[
                    ListTile(
                      title: new Text(
                        'Pengajuan dana anda telah disetujui',
                        style: new TextStyle(
                            color: Colors.white,
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold),
                      ),
                      subtitle: new Text(
                        'Deadline: 3 November 2018 (3 hari lagi)',
                        style: new TextStyle(
                            color: Colors.white70, fontSize: 10.0),
                      ),
                      trailing: new Icon(
                        Icons.alarm_on,
                        color: Colors.white,
                        size: 30.0,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
              child: new Container(
                decoration: new BoxDecoration(
                  color: Color(0xFFF60024),
                ),
                height: 70.0,
                child: ListView(
                  children: <Widget>[
                    ListTile(
                      title: new Text(
                        'Anda memiliki 1 penugasan yang belum selesai',
                        style: new TextStyle(
                            color: Colors.white,
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold),
                      ),
                      subtitle: new Text(
                        'Deadline: 31 Oktober 2018 (1 hari lagi)',
                        style: new TextStyle(
                            color: Colors.white70, fontSize: 10.0),
                      ),
                      trailing: new Icon(
                        Icons.notification_important,
                        color: Colors.white,
                        size: 30.0,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              child: new Container(
                color: Colors.white70,
                height: 70.0,
                child: ListView(
                  children: <Widget>[
                    ListTile(
                      title: new Text(
                        '(1) Pengumuman baru dari sekretariat',
                        style: new TextStyle(
                            color: Colors.black,
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold),
                      ),
                      subtitle: new Text(
                        '1 jam yang lalu',
                        style: new TextStyle(
                            color: Colors.black54, fontSize: 10.0),
                      ),
                      trailing: new Icon(
                        Icons.mic,
                        color: Colors.black,
                        size: 30.0,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
              child: new Container(
                color: Colors.white70,
                height: 70.0,
                child: ListView(
                  children: <Widget>[
                    ListTile(
                      title: new Text(
                        'Johannis Rumimper mencatat 5 pendukung baru',
                        style: new TextStyle(
                            color: Colors.black,
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold),
                      ),
                      subtitle: new Text(
                        '6 jam yang lalu',
                        style: new TextStyle(
                            color: Colors.black54, fontSize: 10.0),
                      ),
                      trailing: new Icon(
                        Icons.book,
                        color: Colors.black,
                        size: 30.0,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
