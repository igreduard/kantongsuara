import 'package:flutter/material.dart';

import 'pages/task/task_onprogress.dart';
import 'pages/task/task_miss.dart';
import 'pages/task/task_details.dart';

class TaskPage extends StatefulWidget {
  @override
  _TaskPageState createState() => _TaskPageState();
}

class _TaskPageState extends State<TaskPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          color: Color(0xFFFCF3C5),
        ),
        child: new ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  top: 10.0, left: 10.0, right: 5.0, bottom: 5.0),
              child: Container(
                decoration: new BoxDecoration(
                    border: new Border(
                        bottom: BorderSide(width: 0.5, color: Colors.black38))),
                alignment: Alignment.centerLeft,
                height: 30.0,
                child: new Text(
                  'Sementara berjalan >',
                  style: new TextStyle(
                      color: Colors.black54,
                      //fontWeight: FontWeight.bold,
                      fontSize: 15.0),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
              child: new Container(
                child: new Column(
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        Expanded(
                          child: new Container(
                            height: 220.0,
                            decoration:
                                new BoxDecoration(color: Colors.transparent),
                            child: new ListView.builder(
                              padding: EdgeInsets.all(4.0),
                              itemBuilder: (context, i) {
                                return new ListTile(
                                  onTap: () => Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              new TaskDetailsPage())),
                                  title: new Text(
                                    'Sekretariat : Survey DPT Kecamatan Airmadidi',
                                    style: new TextStyle(
                                        color: Colors.black87,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  subtitle: new Text(
                                    'Deadline: 3 November 2018',
                                    style: new TextStyle(
                                        color: Colors.black54, fontSize: 10.0),
                                  ),
                                  trailing: new Icon(
                                    Icons.bookmark_border,
                                    color: Colors.black,
                                    size: 30.0,
                                  ),
                                );
                              },
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
              child: new FlatButton(
                color: Colors.transparent,
                onPressed: () => Navigator.of(context).push(
                    new MaterialPageRoute(
                        builder: (BuildContext context) =>
                            new OnProgressPage())),
                child: new Text(
                  'Lihat semua',
                  style: new TextStyle(
                      //fontStyle: FontStyle.italic,
                      color: Colors.black54),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 10.0, left: 10.0, right: 5.0, bottom: 5.0),
              child: Container(
                decoration: new BoxDecoration(
                    border: new Border(
                        bottom: BorderSide(width: 0.5, color: Colors.black38))),
                alignment: Alignment.centerLeft,
                height: 30.0,
                child: new Text(
                  'Terlewatkan >',
                  style: new TextStyle(
                      color: Colors.black54,
                      //fontWeight: FontWeight.bold,
                      fontSize: 15.0),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
              child: new Container(
                child: new Column(
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        Expanded(
                          child: new Container(
                            height: 150.0,
                            decoration:
                                new BoxDecoration(color: Colors.transparent),
                            child: new ListView.builder(
                              padding: EdgeInsets.all(4.0),
                              itemBuilder: (context, i) {
                                return new ListTile(
                                  onTap: () => Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              new TaskDetailsPage())),
                                  title: new Text(
                                    'Caleg : Kampanye Dapil 1',
                                    style: new TextStyle(
                                        color: Colors.black87,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  subtitle: new Text(
                                    'Deadline: 25 Oktober 2018',
                                    style: new TextStyle(
                                        color: Colors.black54, fontSize: 10.0),
                                  ),
                                  trailing: new Icon(
                                    Icons.remove_circle_outline,
                                    color: Colors.black,
                                    size: 30.0,
                                  ),
                                );
                              },
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
              child: new FlatButton(
                color: Colors.transparent,
                onPressed: () => Navigator.of(context).push(
                    new MaterialPageRoute(
                        builder: (BuildContext context) => new MissPage())),
                child: new Text(
                  'Lihat semua',
                  style: new TextStyle(
                      //fontStyle: FontStyle.italic,
                      color: Colors.black54),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
