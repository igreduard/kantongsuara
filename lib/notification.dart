import 'package:flutter/material.dart';

import 'pages/notification/notifications.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          color: Color(0xFFFCF3C5),
        ),
        child: new ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  top: 10.0, left: 10.0, right: 5.0, bottom: 5.0),
              child: Container(
                decoration: new BoxDecoration(
                    border: new Border(
                        bottom: BorderSide(width: 0.5, color: Colors.black38))),
                alignment: Alignment.centerLeft,
                height: 30.0,
                child: new Text(
                  'Terbaru >',
                  style: new TextStyle(
                      color: Colors.black54,
                      //fontWeight: FontWeight.bold,
                      fontSize: 15.0),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
              child: new Container(
                child: new Column(
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        Expanded(
                          child: new Container(
                            height: 220.0,
                            decoration:
                                new BoxDecoration(color: Colors.transparent),
                            child: new ListView.builder(
                              padding: EdgeInsets.all(4.0),
                              itemBuilder: (context, i) {
                                return new ListTile(
                                  onTap: () => print('Delete'),
                                  title: new Text(
                                    'Rudy Haryanto menambahkan 20 suara di Dapil 1',
                                    style: new TextStyle(
                                        color: Colors.black87,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  subtitle: new Text(
                                    '2 menit lalu',
                                    style: new TextStyle(
                                        color: Colors.black54, fontSize: 10.0),
                                  ),
                                  trailing: new Icon(
                                    Icons.notifications_active,
                                    color: Colors.black,
                                    size: 30.0,
                                  ),
                                );
                              },
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 10.0, left: 10.0, right: 5.0, bottom: 5.0),
              child: Container(
                decoration: new BoxDecoration(
                    border: new Border(
                        bottom: BorderSide(width: 0.5, color: Colors.black38))),
                alignment: Alignment.centerLeft,
                height: 30.0,
                child: new Text(
                  'Terdahulu >',
                  style: new TextStyle(
                      color: Colors.black54,
                      //fontWeight: FontWeight.bold,
                      fontSize: 15.0),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
              child: new Container(
                child: new Column(
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        Expanded(
                          child: new Container(
                            height: 150.0,
                            decoration:
                                new BoxDecoration(color: Colors.transparent),
                            child: new ListView.builder(
                              padding: EdgeInsets.all(4.0),
                              itemBuilder: (context, i) {
                                return new ListTile(
                                  title: new Text(
                                    'Sekretariat menambahkan 2 orang di team Dapil 2 Kecamatan Airmadidi',
                                    style: new TextStyle(
                                        color: Colors.black87,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  subtitle: new Text(
                                    '12 jam lalu',
                                    style: new TextStyle(
                                        color: Colors.black54, fontSize: 10.0),
                                  ),
                                  trailing: new Icon(
                                    Icons.notifications,
                                    color: Colors.black,
                                    size: 30.0,
                                  ),
                                );
                              },
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
              child: new FlatButton(
                color: Colors.transparent,
                onPressed: () => Navigator.of(context).push(
                    new MaterialPageRoute(
                        builder: (BuildContext context) =>
                            new NotificationsPage())),
                child: new Text(
                  'Lihat semua',
                  style: new TextStyle(
                      //fontStyle: FontStyle.italic,
                      color: Colors.black54),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
