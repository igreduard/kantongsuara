import 'package:flutter/material.dart';

import 'task_details.dart';

class MissPage extends StatefulWidget {
  @override
  _MissPageState createState() => _MissPageState();
}

class _MissPageState extends State<MissPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          "Terlewatkan",
        ),
        backgroundColor: Color(0xFF3A0017),
      ),
      body: new Stack(
        children: <Widget>[
          new Container(
            color: Color(0xFFFCF3C5),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: new ListView.builder(
                itemBuilder: (context, i) {
                  return new ListTile(
                    onTap: () => Navigator.of(context).push(
                        new MaterialPageRoute(
                            builder: (BuildContext context) =>
                                new TaskDetailsPage())),
                    title: new Text(
                      'Caleg : Kampanye Dapil 1',
                      style: new TextStyle(
                          color: Colors.black87,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold),
                    ),
                    subtitle: new Text(
                      'Deadline: 25 Oktober 2018',
                      style:
                          new TextStyle(color: Colors.black54, fontSize: 10.0),
                    ),
                    trailing: new Icon(
                      Icons.remove_circle_outline,
                      color: Colors.black,
                      size: 30.0,
                    ),
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
