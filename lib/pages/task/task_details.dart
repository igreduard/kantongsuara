import 'package:flutter/material.dart';

class TaskDetailsPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          'Detail Tugas',
        ),
        backgroundColor: Color(0xFF3A0017),
      ),
      body: new Container(
        decoration: new BoxDecoration(
          color: Color(0xFFFCF3C5),
        ),
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: new ListView(
            children: <Widget>[
              new ListTile(
                title: new Text(
                  'Caleg : Kampanye Dapil 1',
                  style: new TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold
                  ),
                ),
                trailing: new FlatButton(
                  onPressed: () => print("Done"),
                  color: Colors.green,
                  child: new Text(
                    'Selesai',
                    style: new TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
                child: new Container(
                  child: new Text(
                    'Dari: Caleg'
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                child: new Container(
                  child: new Text(
                    'Kepada: Jimmy Salibana'
                  ),
                ),
              ),
               Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                child: new Container(
                  child: new Text(
                    'Status: Lewat jadwal',
                    style: new TextStyle(
                      color: Colors.red
                    ),
                  ),
                ),
              ),
                            Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                child: new Container(
                  child: new Text(
                    'Deadline: 25 Oktober 2018'
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: new Container(
                  child: new Text(
                    'Diharapkan kepada kepala tim sukses agar dapat menghadiri acara Kampanye untuk dapil 1 Kecamatan Airmadidi. Guna untuk bersosialisasi dengan masyarakat.',
                  textAlign: TextAlign.justify,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: new Container(
                  height: 60.0,
                  color: Colors.transparent,
                  child: new ListView(
                    children: <Widget>[
                      ListTile(
                        leading: new Icon(
                          Icons.person,
                          size: 40.0,
                        ),
                        title: new Form(
                          child: new TextFormField(
                            decoration: new InputDecoration(
                              hintText: 'Komentar disini'
                            ),
                          ),
                        ),
                        trailing: Container(
                          height: 30.0,
                          width: 70.0,
                          child: new FlatButton(
                            color: Colors.blue,
                            onPressed: () => print('Kirim Komentar'),
                            child: new Text(
                              'Kirim',
                              style: new TextStyle(
                                color: Colors.white
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
              padding: EdgeInsets.only(top: 20.0),
                child: new Container(
                  height: 300.0,
                  color: Colors.transparent,
                  child: new ListView.builder(
                    itemBuilder: (context, i){
                      return new ListTile(
                        leading: new Icon(
                          Icons.account_circle,
                          size: 40.0,
                          color: Colors.black87,
                        ),
                        title: new Text(
                          'Jimmy Salibana',
                          style: new TextStyle(
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        subtitle: new Text(
                          'Mohon maaf pak, kita berhalangan hadir pada waktu itu. Soalnya tpe bini ad maso rumah sakit.'
                        ),
                        trailing: new Text(
                          '1 jam lalu',
                          style: new TextStyle(
                            fontSize: 10.0,
                            color: Colors.grey
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}