import 'package:flutter/material.dart';

class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          "Semua notifikasi",
        ),
        backgroundColor: Color(0xFF3A0017),
      ),
      body: new Stack(
        children: <Widget>[
          new Container(
            color: Color(0xFFFCF3C5),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: new ListView.builder(
                itemBuilder: (context, i) {
                  return new ListTile(
                    onTap: () => print('Delete'),
                    title: new Text(
                      'Rudy Haryanto menambahkan 20 suara di Dapil 1 Kecamatan Airmadidi',
                      style: new TextStyle(
                          color: Colors.black87,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold),
                    ),
                    subtitle: new Text(
                      '1 jam lalu',
                      style:
                          new TextStyle(color: Colors.black54, fontSize: 10.0),
                    ),
                    trailing: new Icon(
                      Icons.notifications,
                      color: Colors.black,
                      size: 30.0,
                    ),
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
