import 'package:flutter/material.dart';

import 'keuangan/keuangan_mohon.dart';
import 'keuangan/mohon_details.dart';
import 'keuangan/permintaan_list.dart';

class KeuanganPage extends StatefulWidget {
  @override
  _KeuanganPageState createState() => _KeuanganPageState();
}

class _KeuanganPageState extends State<KeuanganPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          "Keuangan",
        ),
        backgroundColor: Color(0xFF3A0017),
      ),
      body: new Stack(
        children: <Widget>[
          new Container(
            color: Color(0xFFFCF3C5),
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: new ListView(
                children: <Widget>[
                  Container(
                    height: 60.0,
                    color: Colors.white54,
                    child: new ListTile(
                      title: new Text(
                        'Request Dana',
                        style: new TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                      ),
                      trailing: new Container(
                        height: 30.0,
                        width: 90.0,
                        child: new FlatButton(
                          color: Colors.blue,
                          onPressed: () => Navigator.of(context).push(
                              new MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      new MohonPage())),
                          child: new Text(
                            'Request',
                            style: new TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 75.0, left: 8.0, right: 8.0),
            child: Container(
              decoration: new BoxDecoration(
                  border: new Border(
                      bottom: BorderSide(width: 0.5, color: Colors.black38))),
              alignment: Alignment.centerLeft,
              height: 30.0,
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Text('Daftar request >'),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).push(
                        new MaterialPageRoute(
                            builder: (BuildContext context) =>
                                new PermintaanListPage())),
                    child: new Text(
                      'Lihat semua',
                      style: new TextStyle(color: Colors.grey, fontSize: 12.0),
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 100.0, left: 8.0, right: 8.0),
            child: new Column(
              children: <Widget>[
                Expanded(
                  child: new Container(
                    color: Colors.transparent,
                    child: new ListView.builder(
                      itemBuilder: (context, i) {
                        return Container(
                          height: 60.0,
                          child: new ListTile(
                            onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new MohonDetailsPage())),
                            leading: new Icon(
                              Icons.account_circle,
                              size: 40.0,
                            ),
                            title: new Text(
                              'Ronald Tambengi',
                              style: new TextStyle(fontWeight: FontWeight.bold),
                            ),
                            subtitle: new Text(
                              'Request dana Kampanye Dapil 1',
                              style: new TextStyle(color: Colors.grey),
                            ),
                            trailing: new Text(
                              '1 jam lalu',
                              style: new TextStyle(
                                  fontSize: 10.0, color: Colors.grey),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0, left: 8.0, right: 8.0),
                  child: Container(
                    decoration: new BoxDecoration(
                        border: new Border(
                            bottom:
                                BorderSide(width: 0.5, color: Colors.black38))),
                    alignment: Alignment.centerLeft,
                    height: 30.0,
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text('Daftar request anda >'),
                        new Text(
                          'Lihat semua',
                          style:
                              new TextStyle(color: Colors.grey, fontSize: 12.0),
                        )
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: new Container(
                    color: Colors.transparent,
                    child: new ListView.builder(
                      itemBuilder: (context, i) {
                        return Container(
                          height: 60.0,
                          child: new ListTile(
                            leading: new Icon(
                              Icons.account_circle,
                              size: 40.0,
                            ),
                            title: new Text(
                              'Ronald Tambengi',
                              style: new TextStyle(fontWeight: FontWeight.bold),
                            ),
                            subtitle: new Text(
                              'Permintaan dana Kampanye Dapil 1',
                              style: new TextStyle(color: Colors.grey),
                            ),
                            trailing: new Text(
                              '1 jam lalu',
                              style: new TextStyle(
                                  fontSize: 10.0, color: Colors.grey),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
