import 'package:flutter/material.dart';

class PermintaanListPage extends StatefulWidget {
  @override
  _PermintaanListPageState createState() => _PermintaanListPageState();
}

class _PermintaanListPageState extends State<PermintaanListPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          "Daftar request",
        ),
        backgroundColor: Color(0xFF3A0017),
      ),
      body: new Container(
        color: Color(0xFFFCF3C5),
        child: new ListView.builder(
          itemBuilder: (context, i) {
            return Container(
              height: 60.0,
              child: new ListTile(
                leading: new Icon(
                  Icons.account_circle,
                  size: 40.0,
                ),
                title: new Text(
                  'Ronald Tambengi',
                  style: new TextStyle(fontWeight: FontWeight.bold),
                ),
                subtitle: new Text(
                  'Request dana Kampanye Dapil 1',
                  style: new TextStyle(color: Colors.grey),
                ),
                trailing: new Text(
                  '1 jam lalu',
                  style: new TextStyle(
                    fontSize: 10.0,
                    color: Colors.grey
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
