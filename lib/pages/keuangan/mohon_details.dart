import 'package:flutter/material.dart';

class MohonDetailsPage extends StatefulWidget {
  @override
  _MohonDetailsPageState createState() => _MohonDetailsPageState();
}

class _MohonDetailsPageState extends State<MohonDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
            appBar: new AppBar(
        title: new Text(
          "Request Details",
        ),
        backgroundColor: Color(0xFF3A0017),
      ),
      body: new Container(
        color: Color(0xFFFCF3C5),
        child: new Stack(
          children: <Widget>[
            new Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: new Container(
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text(
                              'Ronald Tambengi',
                              style: new TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold
                              ),
                            ),
                            new Text(
                              'Group : Tim Sukses Dapil 1',
                              style: new TextStyle(
                                fontSize: 15.0
                              ),
                            ),
                            new Text(
                              'Wilayah : Airmadidi',
                              style: new TextStyle(
                                fontSize: 15.0
                              ),
                            ),
                                                        new Text(
                              'Tanggal Request : 3 November 2018',
                              style: new TextStyle(
                                fontSize: 13.0,
                                color: Colors.green
                              ),
                            ),
                          ],
                        ),
                        new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              height: 30.0,
                              child: new FlatButton(
                                onPressed: () => print('Terima'),
                                color: Colors.blue,
                                child: new Text(
                                  'Terima',
                                  style: new TextStyle(
                                    color: Colors.white
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 5.0),
                            ),
                                                        new Container(
                                                          height: 30.0,
                              child: new FlatButton(
                                onPressed: () => print('Tolak'),
                                color: Colors.redAccent,
                                child: new Text(
                                  'Tolak',
                                  style: new TextStyle(
                                    color: Colors.white
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 120.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    'Judul: Dana kampanye Dapil 1 Kecamatan Airmadidi',
                    style: new TextStyle(
                      fontWeight: FontWeight.bold
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.0),
                  ),
                  new Text(
                    'Selamat pagi pak, saya ingin membuat request dana sebesar yang telah saya cantumkan untuk keperluan kampanye dapil 1 wilayah kecamatan airmadidi.'
                  ),
                                    Padding(
                    padding: EdgeInsets.only(top: 10.0),
                  ),
                                    new Text(
                    'Request dana : Rp. 10.000.000',
                    style: new TextStyle(
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}