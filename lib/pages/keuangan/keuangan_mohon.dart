import 'package:flutter/material.dart';

class MohonPage extends StatefulWidget {
  @override
  _MohonPageState createState() => _MohonPageState();
}

class _MohonPageState extends State<MohonPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
            appBar: new AppBar(
        title: new Text(
          "Request Dana",
        ),
        backgroundColor: Color(0xFF3A0017),
      ),
      body: new Stack(
        children: <Widget>[
          new Container(
            color: Color(0xFFFCF3C5),
            child: Padding(
              padding: EdgeInsets.all(20.0),
              child: new Column(
                children: <Widget>[
                                    new Container(
                    child: new Column(
                      children: <Widget>[
                        new Form(
                          child: Container(
                            decoration: new BoxDecoration(
                              color: Colors.white54,
                              border: Border.all(color: Colors.black12)
                            ),
                            height: 55.0,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 5.0, left: 15.0, right: 15.0),
                              child: new TextFormField(
                                decoration: new InputDecoration(
                                  hintText: 'Rp.',
                                  border: InputBorder.none
                                ),
                                maxLines: null,
                                keyboardType: TextInputType.multiline,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                                    Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                  ),
                  new Container(
                    child: new Column(
                      children: <Widget>[
                        new Form(
                          child: Container(
                            decoration: new BoxDecoration(
                              color: Colors.white54,
                              border: Border.all(color: Colors.black12)
                            ),
                            height: 55.0,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 5.0, left: 15.0, right: 15.0),
                              child: new TextFormField(
                                decoration: new InputDecoration(
                                  hintText: 'Judul...',
                                  border: InputBorder.none
                                ),
                                maxLines: null,
                                keyboardType: TextInputType.multiline,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                  ),
                  new Container(
                    child: new Column(
                      children: <Widget>[
                        new Form(
                          child: Container(
                            decoration: new BoxDecoration(
                              color: Colors.white54,
                              border: Border.all(color: Colors.black12)
                            ),
                            height: 200.0,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 5.0, left: 15.0, right: 15.0),
                              child: new TextFormField(
                                decoration: new InputDecoration(
                                  hintText: 'Masukkan keterangan...',
                                  border: InputBorder.none
                                ),
                                maxLines: null,
                                keyboardType: TextInputType.multiline,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                                                      Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                  ),
                                    new Container(
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new Container(
                          height: 40.0,
                          width: 100.0,
                          child: new FlatButton(
                            color: Colors.blue,
                            onPressed: () => print('Kirim'),
                            child: new Text(
                              'Kirim',
                              style: new TextStyle(
                                color: Colors.white
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}