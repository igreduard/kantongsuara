import 'package:flutter/material.dart';

import 'menu.dart' as menupage;
import 'task.dart' as taskpage;
import 'notification.dart' as notificationpage;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin{

  TabController controller;

  @override
  void initState() {
    controller = new TabController(vsync: this, length: 3);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        child: new TabBarView(
          controller: controller,
          children: <Widget>[
            new menupage.MenuPage(),
            new taskpage.TaskPage(),
            new notificationpage.NotificationPage()
          ],
        ),
      ),
      bottomNavigationBar: Container(
        color: Color(0xFF3A0017),
        child: new TabBar(
          labelColor: Colors.orange,
          indicatorColor: Colors.transparent,
          controller: controller,
          unselectedLabelColor: Colors.white,
          tabs: <Widget>[
            new Tab(icon: new Icon(Icons.home),),
            new Tab(icon: new Icon(Icons.format_list_bulleted),),
            new Tab(icon: new Icon(Icons.notifications),),
          ],
        ),
      ),
      
    );
  }
}